package me.demo.currency.app.presentation.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import me.demo.currency.R
import me.demo.currency.core.domain.DisplayRate

class DisplayRateAdapter: RecyclerView.Adapter<DisplayRateViewHolder>() {

    private var items: List<DisplayRate> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DisplayRateViewHolder {
        val inflatedView = LayoutInflater.from(parent.context)
            .inflate(R.layout.main_currency_row, parent, false)
        return DisplayRateViewHolder(inflatedView)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: DisplayRateViewHolder, position: Int) = holder.bind(items[position])

    fun update(items: List<DisplayRate>){
        if (this.items != items){
            this.items = items
            notifyDataSetChanged()
        }
    }
}