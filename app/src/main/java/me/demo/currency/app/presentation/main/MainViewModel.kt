package me.demo.currency.app.presentation.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import me.demo.currency.app.framework.util.combineLatest
import me.demo.currency.app.framework.util.map
import me.demo.currency.core.domain.DisplayRate
import me.demo.currency.core.domain.ExchangeRate
import me.demo.currency.core.interactors.CalculateQuantities
import me.demo.currency.core.interactors.FetchExchangeRates
import timber.log.Timber

private const val MINUTE = 60 * 1000L

class MainViewModel(
    private val fetch: FetchExchangeRates,
    private val calculate: CalculateQuantities
) : ViewModel() {

    private val amount = MutableLiveData<Double>().apply { value = 0.0 }
    private val rates = MutableLiveData<List<ExchangeRate>>().apply { value = emptyList() }
    private val displayRates = rates
        .combineLatest(amount)
        .map { (list, value) -> calculate(list, value) }

    fun startUpdate(scope: CoroutineScope) {
        scope.launch {
            while (true) {
                Timber.d("Fetching rates once a minute...")
                try {
                    fetch().apply { rates.postValue(this) }
                } catch (e: Exception) {
                    Timber.e(e)
                }
                delay(MINUTE)
            }
        }
    }

    fun setAmount(amount: Double) = this.amount.postValue(amount)

    fun getDisplayRates(): LiveData<List<DisplayRate>> = displayRates

}
