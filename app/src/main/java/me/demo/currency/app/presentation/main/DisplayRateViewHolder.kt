package me.demo.currency.app.presentation.main

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import me.demo.currency.R
import me.demo.currency.core.domain.DisplayRate
import me.demo.currency.core.util.to2DecimalsString

class DisplayRateViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    private val name: TextView = view.findViewById(R.id.name)
    private val rate: TextView = view.findViewById(R.id.rate)
    private val quantity: TextView = view.findViewById(R.id.quantity)

    fun bind(item: DisplayRate){
        name.text = item.name
        rate.text = item.exchangeRate.to2DecimalsString()
        quantity.text = item.quantity.to2DecimalsString()
    }

}