package me.demo.currency.app.framework.network

import me.demo.currency.core.data.ExchangeRateDataSource
import me.demo.currency.core.domain.ExchangeRate

class RetrofitExchangeRatesDataSource(private val api: CurrencyService) : ExchangeRateDataSource {
    override suspend fun fetch(): List<ExchangeRate> = api.fetchExchangeRates()
}