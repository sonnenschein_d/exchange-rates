package me.demo.currency.app.presentation.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import me.demo.currency.R
import me.demo.currency.app.framework.util.afterTextChanged
import me.demo.currency.core.domain.DisplayRate
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var list: RecyclerView
    private lateinit var amount: EditText

    private val adapter: DisplayRateAdapter by inject()
    private val viewModel: MainViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews(view)
        setupAmount()
        setupList()
        observeRates()
        startUpdate()
    }

    private fun initViews(view: View) {
        list = view.findViewById(R.id.list)
        amount = view.findViewById(R.id.amount)
    }

    private fun setupAmount() {
        amount.afterTextChanged {
            it.toDoubleOrNull()?.let { viewModel.setAmount(it) }
        }
    }

    private fun setupList() {
        list.layoutManager = LinearLayoutManager(activity)
        list.setHasFixedSize(true)
        list.adapter = adapter
    }

    private fun observeRates() {
        viewModel.getDisplayRates().observe(viewLifecycleOwner, Observer<List<DisplayRate>> {
            adapter.update(it)
        })
    }

    private fun startUpdate() {
        viewLifecycleOwner.lifecycle.addObserver(
            UpdateLifecycleObserver(viewModel::startUpdate)
        )
    }

}
