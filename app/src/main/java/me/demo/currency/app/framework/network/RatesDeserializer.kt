package me.demo.currency.app.framework.network

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonParseException
import me.demo.currency.core.domain.ExchangeRate
import java.lang.reflect.Type


class RatesDeserializer : JsonDeserializer<List<ExchangeRate>> {
    override fun deserialize(
        json: JsonElement?,
        typeOfT: Type?,
        context: JsonDeserializationContext?
    ): List<ExchangeRate> {
        val root = json?.getAsJsonObject() ?: fail("Null response")
        val currencies = root.get("Valute")?.asJsonObject ?: fail("Null 'valute' element")
        return currencies.entrySet().toList().map { (_, entry) ->
            with(entry.asJsonObject) {
                ExchangeRate(
                    get("ID")?.asString ?: fail("Null 'ID'"),
                    get("NumCode")?.asInt ?: fail("Null 'NumCode'"),
                    get("CharCode")?.asString ?: fail("Null 'CharCode'"),
                    get("Nominal")?.asInt ?: fail("Null 'Nominal'"),
                    get("Name")?.asString ?: fail("Null 'Name'"),
                    get("Value")?.asDouble ?: fail("Null 'Value'"),
                    get("Previous")?.asDouble ?: fail("Null 'Previous'")
                )
            }
        }
    }

    private fun fail(msg: String): Nothing = throw JsonParseException(msg)
}