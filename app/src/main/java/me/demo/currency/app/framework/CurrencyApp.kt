package me.demo.currency.app.framework

import android.app.Application
import me.demo.currency.BuildConfig
import me.demo.currency.app.framework.di.mainModule
import me.demo.currency.app.framework.di.networkModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber

class CurrencyApp: Application() {

    override fun onCreate() {
        super.onCreate()
        initTimber()
        initKoin()
    }

    private fun initTimber(){
        if (BuildConfig.DEBUG){
            Timber.plant(Timber.DebugTree())
        }
    }

    private fun initKoin() = startKoin {
        androidContext(this@CurrencyApp)
        modules(listOf(mainModule, networkModule))
    }

}