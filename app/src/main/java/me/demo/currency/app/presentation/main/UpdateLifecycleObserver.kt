package me.demo.currency.app.presentation.main

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.cancel
import timber.log.Timber

class UpdateLifecycleObserver(private val startUpdate: (CoroutineScope) -> Unit): LifecycleObserver {

    private lateinit var scope: CoroutineScope

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun start() {
        Timber.d("start update")
        scope = MainScope()
        startUpdate(scope)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun stop() {
        Timber.d("stop update")
        scope.cancel()
    }

}