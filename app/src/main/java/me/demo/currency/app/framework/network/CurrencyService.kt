package me.demo.currency.app.framework.network

import me.demo.currency.core.domain.ExchangeRate
import retrofit2.http.GET

interface CurrencyService {

    @GET("/daily_json.js")
    suspend fun fetchExchangeRates(): List<ExchangeRate>

}