package me.demo.currency.app.framework.di

import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import me.demo.currency.app.framework.network.CurrencyService
import me.demo.currency.app.framework.network.RatesDeserializer
import me.demo.currency.app.framework.network.RetrofitExchangeRatesDataSource
import me.demo.currency.app.presentation.main.DisplayRateAdapter
import me.demo.currency.app.presentation.main.MainViewModel
import me.demo.currency.core.data.ExchangeRateDataRepository
import me.demo.currency.core.data.ExchangeRateDataSource
import me.demo.currency.core.domain.ExchangeRate
import me.demo.currency.core.interactors.CalculateQuantities
import me.demo.currency.core.interactors.FetchExchangeRates
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


val mainModule = module {

    factory { CalculateQuantities() }
    factory { DisplayRateAdapter() }
    viewModel { MainViewModel(get(), get()) }
}

val networkModule = module {
    factory { RetrofitExchangeRatesDataSource(get()) as ExchangeRateDataSource }
    factory { ExchangeRateDataRepository(get()) }
    factory { FetchExchangeRates(get()) }

    single {
        val ratesListType = object : TypeToken<MutableList<ExchangeRate>>() {}.type
        GsonBuilder()
            .registerTypeAdapter(ratesListType, RatesDeserializer())
            .create()
    }

    single {
        GsonConverterFactory.create(get()) as Converter.Factory
    }

    single {
        OkHttpClient.Builder()
            .addInterceptor(HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BASIC
            })
            .build()
    }

    single {
        Retrofit.Builder()
            .baseUrl("http://www.cbr-xml-daily.ru")
            .addConverterFactory(get())
            .client(get())
            .build().create(CurrencyService::class.java)
    }
}