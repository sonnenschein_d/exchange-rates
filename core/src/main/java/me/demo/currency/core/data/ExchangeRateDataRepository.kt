package me.demo.currency.core.data

import me.demo.currency.core.domain.ExchangeRate

class ExchangeRateDataRepository(private val dataSource: ExchangeRateDataSource) {
    suspend fun fetchCurrency(): List<ExchangeRate> = dataSource.fetch()
}