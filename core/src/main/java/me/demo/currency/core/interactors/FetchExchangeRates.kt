package me.demo.currency.core.interactors

import me.demo.currency.core.data.ExchangeRateDataRepository
import me.demo.currency.core.domain.ExchangeRate

class FetchExchangeRates(private val repository: ExchangeRateDataRepository) {
    suspend operator fun invoke(): List<ExchangeRate> = repository.fetchCurrency()
}