package me.demo.currency.core.domain

data class ExchangeRate(val id: String,
                        val numCode: Int,
                        val charCode: String,
                        val nominal: Int,
                        val name: String,
                        val value: Double,
                        val previous: Double)