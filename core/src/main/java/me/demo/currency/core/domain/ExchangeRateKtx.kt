package me.demo.currency.core.domain

private fun ExchangeRate.displayName() = if (nominal == 1) name else "$nominal $name"
private fun ExchangeRate.quantity(rubles: Double) = nominal * rubles / value

fun ExchangeRate.toDisplayCurrency(rubles: Double): DisplayRate
        = DisplayRate(displayName(), value, quantity(rubles))