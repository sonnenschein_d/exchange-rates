package me.demo.currency.core.data

import me.demo.currency.core.domain.ExchangeRate

interface ExchangeRateDataSource {
    suspend fun fetch(): List<ExchangeRate>
}