package me.demo.currency.core.util

fun Double.to2DecimalsString(): String = "%.2f".format(this)