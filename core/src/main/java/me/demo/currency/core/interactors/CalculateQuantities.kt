package me.demo.currency.core.interactors

import me.demo.currency.core.domain.ExchangeRate
import me.demo.currency.core.domain.DisplayRate
import me.demo.currency.core.domain.toDisplayCurrency

class CalculateQuantities {
    operator fun invoke(list: List<ExchangeRate>, rubles: Double): List<DisplayRate>
            = list.map { it.toDisplayCurrency(rubles) }
}