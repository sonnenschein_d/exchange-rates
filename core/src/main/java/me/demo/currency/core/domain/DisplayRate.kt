package me.demo.currency.core.domain

data class DisplayRate(val name: String,
                       val exchangeRate: Double,
                       val quantity: Double)